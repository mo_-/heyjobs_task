# frozen_string_literal: true

require_relative '../../config/application'

RSpec.describe Campaign, type: :model do
  describe 'Associations' do
    it { should belong_to(:job) }
  end

  describe 'Validations' do
    it { should validate_uniqueness_of(:external_reference).case_insensitive }
  end
end
