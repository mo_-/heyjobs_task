# frozen_string_literal: true

require_relative '../../config/application'

RSpec.describe Job, type: :model do
  describe 'Associations' do
    it { should have_many(:campaigns) }
  end
end
