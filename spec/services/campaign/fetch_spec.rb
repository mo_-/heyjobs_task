# frozen_string_literal: true

require_relative '../../../config/application'
require_relative '../../fixtures/fake'

RSpec.describe do
  describe Services::Campaign::Fetch do
    let(:response) { Fake::ADS }

    let(:url) { Services::Campaign::Fetch::DEFAULT_URL }

    describe '#perform' do
      it 'performs http request and returns json data from external url' do
        stub_request(:get, url)
          .to_return(status: 200, body: response.to_json)
        result = Services::Campaign::Fetch.new(url).perform

        expect(result).to eq(response)
      end
    end
  end
end
