# frozen_string_literal: true

require_relative '../../../config/application'
require_relative '../../fixtures/fake'

RSpec.describe do
  describe Services::Campaign::DetectDiscrepancy do
    let(:ads) { Fake::ADS }
    let(:expected_result) do
      {
        'remote_reference': '1',
        'discrepancies': [
          'status': {
            'remote': 'enabled',
            'local': 'active'
          },
          'description': {
            'remote': 'Description for campaign 11',
            'local': 'Description for campaign 1'
          }
        ]
      }
    end

    describe '#perform' do
      it "detects descrepancies between local and remote Ad's campaigns
        and resturns differences" do
        result = described_class.new(ads).perform

        expect(result).not_to be(nil)
        expect(result).to include(expected_result)
      end
    end
  end
end
