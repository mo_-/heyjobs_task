# frozen_string_literal: true

require 'active_record'

ActiveRecord::Base.establish_connection(
  adapter: 'postgresql',
  database: 'development'
)

require 'httparty'
require 'json'
require 'pry'

require_relative '../lib/models/application_record'
require_relative '../lib/models/job'
require_relative '../lib/models/campaign'

require_relative '../lib/services/campaign/fetch'
require_relative '../lib/services/campaign/detect_descrepancy'
