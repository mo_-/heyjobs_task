# frozen_string_literal: true

# Service to detect discrepancies
# between ad campaigns from 3rd party' service and local campaigns saved in our DB
module Services
  module Campaign
    class DetectDiscrepancy
      attr_reader :ads

      def initialize(ads)
        @ads = ads
      end

      def self.perform(ads)
        new(ads).perform
      end

      def perform
        return error if ads.blank?

        local_campaigns
        descrepancies
      end

      private

      def error
        raise ArgumentError, ads_should_not_be_empty_message
      end

      def local_campaigns
        @local_campaigns ||= ::Campaign.where(external_reference: references)
      end

      def descrepancies
        result = []
        local_campaigns.each do |campaign|
          result << descrepancy_for(campaign)
        end
        result
      end

      def descrepancy_for(campaign)
        {
          remote_reference: campaign.external_reference,
          discrepancies: [
            status: {
              remote: remote_status_by(campaign.external_reference),
              local: campaign.status
            },
            description: {
              remote: remote_description_by(campaign.external_reference),
              local: campaign.ad_description
            }
          ]
        }
      end

      def references
        ads['ads'].map { |i| i['reference'] }
      end

      def remote_status_by(reference)
        ads['ads']
          .detect { |ad| ad['reference'] == reference }
          .slice('status')
          .try('[]', 'status')
      end

      def remote_description_by(reference)
        ads['ads']
          .detect { |ad| ad['reference'] == reference }
          .slice('description')
          .try('[]', 'description')
      end

      def ads_should_not_be_empty_message
        "Failed to perform detection, 'ads' argument should not be empty"
      end
    end
  end
end
