# frozen_string_literal: true

# Service to fetch ad campaigns from external endpoint
module Services
  module Campaign
    class Fetch
      include HTTParty

      attr_reader :ad_service_url

      DEFAULT_URL = 'https://mockbin.org/bin/fcb30500-7b98-476f-810d-463a0b8fc3df'

      def initialize(ad_service_url)
        @ad_service_url = ad_service_url
      end

      def self.perform(url)
        new(url).perform
      end

      def perform
        puts "Fetching data from #{ad_service_url}"
        return unless response.success?

        JSON.parse(response)
      end

      def response
        @_response = self.class.get(ad_service_url)
      end
    end
  end
end
