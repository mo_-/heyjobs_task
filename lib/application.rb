# frozen_string_literal: true

require_relative '../config/application'

class Application
  attr_reader :remote_url, :ads

  def initialize(remote_url = default_url)
    @remote_url = remote_url
    @ads ||= {}
  end

  def run
    fetch_campaigns
    detect_descrepancies
  end

  def fetch_campaigns
    @ads = Services::Campaign::Fetch
           .perform(remote_url)
  end

  def detect_descrepancies
    Services::Campaign::DetectDiscrepancy.perform(ads)
  end

  private

  def default_url
    Services::Campaign::Fetch::DEFAULT_URL
  end
end
