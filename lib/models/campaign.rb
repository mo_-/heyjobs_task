# frozen_string_literal: true

class Campaign < ApplicationRecord
  belongs_to :job

  validates_uniqueness_of :external_reference, case_sensitive: false
end
