# frozen_string_literal: true

class CreateCampaigns < ActiveRecord::Migration[5.1]
  def change
    create_table :campaigns do |t|
      t.string :status
      t.string :external_reference
      t.text :ad_description
      t.references :job, index: true

      t.timestamps
    end
  end
end
