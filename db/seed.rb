# frozen_string_literal: true

require_relative "../lib/application"


references = [1, 2, 3]

Campaign.delete_all
Job.delete_all

references.each do |ref|
  job = Job.create(title: "Job title #{ref}", details: { description: "Description for job #{ref}"})
  Campaign.create(
    external_reference: ref,
    ad_description: "Description for campaign #{ref}",
    job_id: job.id,
    status: 'active'
  )
end
