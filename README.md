## [Heyjobs Ruby task](https://github.com/heyjobs/ruby-task)' solution.


### My understanding of the task:
We have local ad campaigns which are supposed to be synced with remote ad campaigns from a 3rd party service.

We need to build a service to `fetch` remote ad' campaigns and then `detect` discrepancies between the remote campaigns and our local ad campaigns.


### Brief explanation of the solution:
- The app is structered as explained in the task and I created a `Rakefile` with some tasks to make it easy to use. 
- Created two Activerecord models (`Job` and `Campaign`) — each `job` has many `campaigns`.
- Created two `Service` objects: `Services::Campaign::Fetch` (Responsible for fetching 3rd party remote campaigns) and `Services::Campaign::DetectDiscrepancy` (Responsible to detect discrepancies between remote and local campaigns) and returns the desired output.

**Note**: I assumed when writing the service that each local campaign has a reference to only one remote campaign.


### How to use:
- Clone this repo.
- `cd` into the project folder.
- Create the database: `rake db:create`.
- Migrate the database: `rake db:migrate`.
- Add some data: `rake db:seed`.
- `irb` in the terminal.
- `require_relative "lib/application"`
- `Application.new.run`

### Tests
Run the tests:
`rspec`

